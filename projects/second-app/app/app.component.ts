import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { LayoutService } from '../common/services/layout.service';
import { KeyValueInterface } from '../common/interfaces/key-value.interface';
import { UserModel } from '../common/models/user.model';
import { ActionModel } from '../common/models/action.model';
import { StoreStateModel } from '../common/models/store-state.model';
import { ActionUserEnum } from '../common/enums/store/action-user.enum';
import { ActionUserCacheEnum } from '../common/enums/store/action-user-cache.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})

export class AppComponent {
  public users: UserModel[];

  private userCacheCounter: number = 0;
  private userCounter: number = 0;

  private socket: WebSocket;

  constructor(private store: Store<StoreStateModel>,
              private layoutService: LayoutService) {
    this.layoutService.onBodyClick.subscribe((event: MouseEvent): void => {
      console.log('Click', event);
    });
    this.layoutService.onBodyKeyDown.subscribe((event: KeyboardEvent): void => {
      console.log('KeyDown', event);
    });
    this.layoutService.onBodyKeyUp.subscribe((event: KeyboardEvent): void => {
      console.log('KeyUp', event);
    });

    // Triggers for loading user list. Called 2 times to test the mergeMap function in effect
    // That's the way to call NGRX effect
    this.store.dispatch(new ActionModel({
      type: ActionUserCacheEnum.Load,
      payload: []
    }));
    this.store.dispatch(new ActionModel({
      type: ActionUserCacheEnum.Load,
      payload: []
    }));

    this.initSocket();

    setTimeout(
      (): void => this.sendToSocketServer(JSON.stringify({ a: 1 })),
      2000
    );

    setTimeout(
      (): void => this.sendToSocketServer(JSON.stringify({ b: 2 })),
      4000
    );

    /*this.addUsers();
    this.changeUser();*/
  }

  public addUsers(): void {
    const timer: any = setInterval(
      (): void => {
        this.store.next(new ActionModel<UserModel[]>({
          type: ActionUserCacheEnum.Add,
          payload: [new UserModel({
            id: this.userCacheCounter.toString(),
            name: `User#${this.userCacheCounter}`,
            externalAccounts: []
          })]
        }));

        if (this.userCacheCounter < 10) {
          this.userCacheCounter ++;
        } else {
          this.userCacheCounter = 0;
          clearInterval(timer);
          this.removeUsers();
        }
      },
      2000
    );
  }

  public removeUsers(): void {
    const timer: any = setInterval(
      (): void => {
        this.store.next(new ActionModel<UserModel[]>({
          type: ActionUserCacheEnum.Remove,
          payload: [new UserModel({
            id: this.userCacheCounter.toString(),
            name: '',
            externalAccounts: []
          })]
        }));

        if (this.userCacheCounter < 10) {
          this.userCacheCounter ++;
        } else {
          this.userCacheCounter = 0;
          clearInterval(timer);
          this.addUsers();
        }
      },
      2000
    );
  }

  public changeUser(): void {
    setInterval(
      (): void => {
        this.store.next(new ActionModel<KeyValueInterface<any>>({
          type: ActionUserEnum.Edit,
          payload: {
            name: `LoggedUser#${this.userCounter}`
          }
        }));

        if (this.userCounter > 20) {
          this.userCounter = 0;
        } else {
          this.userCounter ++;
        }
      },
      5000
    );
  }

  private initSocket(): void {
    this.socket = new WebSocket('wss://echo.websocket.org');

    this.socket.onmessage = (message: MessageEvent): void => {
      console.log('onmessage:', message);
    };

    this.socket.onerror = (event: Event): void => {
      console.error('onerror:', event);
    };

    this.socket.onclose = (event: CloseEvent): void => {
      console.log('onclose:', event);

      if (!!this.socket) {
        this.socket.close();
      }

      delete this.socket;

      setTimeout(
        (): void => this.initSocket(),
        10000
      );
    };

    this.socket.onopen = (event: Event): void => {
      console.log('onopen:', event);
    };
  }

  private sendToSocketServer(data: string): void {
    if (!!this.socket) {
      this.socket.send(data);
    }
  }
}
