export interface ExternalAccountInterface {
  id: string;
  serverName: string;
  username: string;
}
