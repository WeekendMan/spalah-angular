export enum ActionUserCacheEnum {
  Add = 'user_cache_add',
  Remove = 'user_cache_remove',
  Load = 'user_cache_load'
}
